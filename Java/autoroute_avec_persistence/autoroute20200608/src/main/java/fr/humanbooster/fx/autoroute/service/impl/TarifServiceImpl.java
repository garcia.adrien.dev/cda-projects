package fr.humanbooster.fx.autoroute.service.impl;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.autoroute.business.Tarif;
import fr.humanbooster.fx.autoroute.dao.TarifDao;
import fr.humanbooster.fx.autoroute.dao.impl.TarifDaoImpl;
import fr.humanbooster.fx.autoroute.service.TarifService;

public class TarifServiceImpl implements TarifService {
	private TarifDao tarifDao = new TarifDaoImpl();


	@Override
	public Tarif ajouterTarif(Tarif tarif) {
			try {
			// On confie l'objet à la DAO
			tarifDao.create(tarif);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tarif;
	}


	@Override
	public Tarif recupererTarif(Long id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Tarif> recupererTarifs() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Tarif supprimerTarif(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Tarif updateTarif(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
