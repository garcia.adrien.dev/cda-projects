package fr.humanbooster.fx.autoroute.service;

import java.util.List;

import fr.humanbooster.fx.autoroute.business.Peage;

public interface PeageService {
		
		public Peage ajouterPeage(String nom);
		public Peage recupererPeage(Long id);
		public List<Peage> recupererPeages();

}
