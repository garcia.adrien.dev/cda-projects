package fr.humanbooster.fx.autoroute.service.impl;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.autoroute.business.Classe;
import fr.humanbooster.fx.autoroute.dao.ClasseDao;
import fr.humanbooster.fx.autoroute.dao.impl.ClasseDaoImpl;
import fr.humanbooster.fx.autoroute.service.ClasseService;


public class ClasseServiceImpl implements ClasseService {
	private ClasseDao classeDao = new ClasseDaoImpl();

	@Override
	public Classe ajouterClasse(String nom) {
		Classe classe = new Classe(nom);
		try {
			classeDao.create(classe);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return classe;
	}

	@Override
	public Classe recupererClasse(Long id) {
		try {
			return classeDao.findOne(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Classe> recupererClasses() {
		try {
			return classeDao.findAll();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}