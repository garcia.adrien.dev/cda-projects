package fr.humanbooster.fx.autoroute.dao;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.autoroute.business.Peage;

public interface PeageDao {
	
	Peage findOne(Long id) throws SQLException;
	
	List<Peage> findAll() throws SQLException;

	Peage create(Peage peage) throws SQLException;
	

}
