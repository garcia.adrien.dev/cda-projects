package fr.humanbooster.fx.autoroute.dao;

import java.sql.SQLException;

import java.util.List;


import fr.humanbooster.fx.autoroute.business.Tarif;

public interface TarifDao {
	
	Tarif findOne(Long id) throws SQLException;
	
	List<Tarif> findAll() throws SQLException;

	boolean delete(Long id) throws SQLException;

	List<Tarif> findAllById(Long id) throws SQLException;

	boolean update(Long id, float montant) throws SQLException;

	Tarif create(Tarif tarif) throws SQLException;
	
	
	

}
