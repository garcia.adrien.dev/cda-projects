package fr.humanbooster.fx.autoroute.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.autoroute.business.Tarif;
import fr.humanbooster.fx.autoroute.dao.ClasseDao;
import fr.humanbooster.fx.autoroute.dao.Connexions;
import fr.humanbooster.fx.autoroute.dao.PeageDao;
import fr.humanbooster.fx.autoroute.dao.Requetes;
import fr.humanbooster.fx.autoroute.dao.TarifDao;

public class TarifDaoImpl implements TarifDao {

	private Connection connexion;
	private ClasseDao classeDao;
	private PeageDao peageDao;

	public TarifDaoImpl() {
		try {
			connexion = Connexions.getConnection();
			classeDao = new ClasseDaoImpl();
			peageDao = new PeageDaoImpl();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		classeDao = new ClasseDaoImpl();
		peageDao = new PeageDaoImpl();
		
	}


	@Override
	public Tarif create(Tarif tarif) throws SQLException {
		PreparedStatement ps = connexion.prepareStatement(Requetes.AJOUT_TARIF, Statement.RETURN_GENERATED_KEYS);
		ps.setFloat(1, tarif.getMontant());
		if (tarif.getDateEffet()!=null) {
            ps.setDate(2, new java.sql.Date(tarif.getDateEffet().getTime()));
        }
        else {
            ps.setDate(2, null);
        }
		ps.setLong(3, tarif.getClasse().getId());
		ps.setLong(4, tarif.getPeageEntree().getId());
		ps.setLong(5, tarif.getPeageSortie().getId());
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		rs.next();
		tarif.setId(rs.getLong(1));
		return tarif;
	}

	@Override
    public Tarif findOne(Long id) throws SQLException {
        Tarif tarif = null;
        PreparedStatement ps = connexion.prepareStatement(Requetes.TARIF_PAR_ID);
        ps.setLong(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
        	tarif = new Tarif();
        	tarif.setId(rs.getLong(1));
        	tarif.setClasse(classeDao.findOne(rs.getLong("classe_id")));
        	tarif.setPeageEntree(peageDao.findOne(rs.getLong("peage_entree_id")));
        	tarif.setPeageSortie(peageDao.findOne(rs.getLong("peage_sortie_id")));
        	
            return tarif;
        }
        return null;
    }

	@Override
    public List<Tarif> findAll() throws SQLException {
        List<Tarif> tarifs = new ArrayList<>();
        PreparedStatement peageService = connexion.prepareStatement(Requetes.TOUS_LES_TARIFS);
        ResultSet rs = peageService.executeQuery();
        while (rs.next()) {
            Tarif tarif = new Tarif();
            tarif.setMontant(rs.getFloat("montant"));
            tarif.setId(rs.getLong("id"));
            tarif.setDateEffet(rs.getDate("dateEffet"));
            tarif.setClasse(classeDao.findOne(rs.getLong("classe_id")));
            tarif.setPeageEntree(peageDao.findOne(rs.getLong("peage_entree_id")));
            tarif.setPeageSortie(peageDao.findOne(rs.getLong("peage_sortie_id")));
            tarifs.add(tarif);
        }
        return tarifs;
    }


	@Override
	public boolean update(Long id, float montant) throws SQLException {
		if (id == null) {
			return false;
		}
		if (findOne(id) != null) {
			PreparedStatement ps = connexion.prepareStatement(Requetes.MODIF_MONTANT_TARIF);
			ps.setFloat(1, montant);
			ps.setLong(2, id);
			ps.executeUpdate();
			return true;
		}
		return false;
	}

	@Override
    public List<Tarif> findAllById(Long id) throws SQLException {
        List<Tarif> tarifs = new ArrayList<>();
        PreparedStatement ps = connexion.prepareStatement(Requetes.TOUS_LES_TARIFS_PAR_PEAGE);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
        	Tarif tarif = new Tarif();
            tarif.setId(rs.getLong("id"));
        	tarif.setClasse(classeDao.findOne(rs.getLong("classe_id")));
        	tarif.setPeageEntree(peageDao.findOne(rs.getLong("peage_entree_id")));
        	tarif.setPeageSortie(peageDao.findOne(rs.getLong("classe_sortie_id")));

            tarifs.add(tarif);
        }
        return tarifs;
    }

	@Override
	public boolean delete(Long id) throws SQLException {
		if (findOne(id) == null)
			return false;
		PreparedStatement ps = connexion.prepareStatement(Requetes.SUPPRESSION_TARIF);
		ps.setLong(1, id);
		ps.executeUpdate();
		return true;
	}

}
