package fr.humanbooster.fx.autoroute.service;

import java.util.List;

import fr.humanbooster.fx.autoroute.business.Classe;

public interface ClasseService {
	
	public Classe ajouterClasse(String nom);
	public Classe recupererClasse(Long id);
	public List<Classe> recupererClasses();
}
