package fr.humanbooster.fx.autoroute.dao;

public class Requetes {

		public static final String AJOUT_CLASSE = "INSERT INTO classe (nom) VALUES (?)";
		public static final String CLASSE_PAR_ID = "SELECT * FROM classe WHERE id = ?";
		public static final String TOUTES_LES_CLASSES = "SELECT * FROM classe";
		
		public static final String AJOUT_PEAGE = "INSERT INTO peage (nom) VALUES (?)";
		public static final String PEAGE_PAR_ID = "SELECT * FROM peage WHERE id = ?";
		public static final String TOUS_LES_PEAGES = "SELECT * FROM peage";
		
	    public static final String AJOUT_TARIF = "INSERT INTO tarif(montant, dateEffet, classe_id, peage_entree_id, peage_sortie_id) VALUES (?,?,?,?,?)";
	    public static final String TARIF_PAR_ID = "SELECT * FROM tarif WHERE id = ?";
	    public static final String TOUS_LES_TARIFS = "SELECT * FROM tarif ORDER BY peage_entree_id, peage_sortie_id, classe_id";
	    public static final String SUPPRESSION_TARIF = "DELETE FROM tarif where id=?";
	    public static final String MODIF_MONTANT_TARIF = "UPDATE tarif SET montant=? WHERE id=?";
	    public static final String TOUS_LES_TARIFS_PAR_PEAGE = "SELECT * FROM tarif LEFT JOIN peage ON peage.id = tarif.peage_entree_id join classe on classe.id = tarif.classe_id ORDER BY peage.nom, classe.nom WHERE peage_entree_id=?";
	}
