package fr.humanbooster.fx.autoroute;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import fr.humanbooster.fx.autoroute.business.Classe;
import fr.humanbooster.fx.autoroute.business.Peage;
import fr.humanbooster.fx.autoroute.business.Tarif;
import fr.humanbooster.fx.autoroute.service.ClasseService;
import fr.humanbooster.fx.autoroute.service.PeageService;
import fr.humanbooster.fx.autoroute.service.TarifService;
import fr.humanbooster.fx.autoroute.service.impl.ClasseServiceImpl;
import fr.humanbooster.fx.autoroute.service.impl.PeageServiceImpl;
import fr.humanbooster.fx.autoroute.service.impl.TarifServiceImpl;

public class App {
			
	private static Scanner scanner = new Scanner(System.in);
	private static ClasseService classeService = new ClasseServiceImpl();
	private static PeageService peageService = new PeageServiceImpl();
	private static TarifService tarifService = new TarifServiceImpl();

	public static void main(String[] args) {
		//on declare un objet de type Instant
		Instant debut = Instant.now();
		Peage p1 = peageService.ajouterPeage("Lyon");
		Instant fin = Instant.now();
		System.out.println(fin.toEpochMilli() - debut.toEpochMilli());
		Peage p2 = peageService.ajouterPeage("grenoble");
		Classe c1 = classeService.ajouterClasse("vehicule leger");
		Tarif t1 = new Tarif();
		t1.setMontant(2);
		t1.setPeageEntree(p1);
		t1.setClasse(c1);
		//TODO Ajout de mille tarifs en choisissant au hasard une classe et 2 peages
		Random random = new Random();
		List<Classe> classes = classeService.recupererClasses();
		List<Peage> peages = peageService.recupererPeages();
		for (int i = 0; i < 10000; i++) {
			Tarif tarif = new Tarif();
			tarif.setMontant(1 + random.nextInt(100));
			//On choisi au hasard une classe de vehicule
			Classe classe = classes.get(random.nextInt(classes.size()));
			Set<Peage> peagesDuTarif = new HashSet<>();
			while (peagesDuTarif.size() != 2) {
				peagesDuTarif.add(peages.get(random.nextInt(peages.size())));
			}
			//On declare une liste construite a partir du set
			List<Peage> listPeages = new ArrayList<>();
			listPeages.addAll(peagesDuTarif);
			Collections.shuffle(listPeages);
			
			Peage peageDepart = listPeages.get(0);
			Peage peageArriver = listPeages.get(1);
			//On associe la classe et les peages au nouveau tarif
			tarif.setClasse(classe);
			tarif.setPeageEntree(peageDepart);
			tarif.setPeageSortie(peageArriver);
			//On confie lobjet tarif au service qui va le rendre persistant
			tarifService.ajouterTarif(tarif);
			
		}
		//System.out.println(t1);
		//System.out.println(p2);
	}
    
}