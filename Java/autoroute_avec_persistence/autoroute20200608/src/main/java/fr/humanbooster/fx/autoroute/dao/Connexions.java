package fr.humanbooster.fx.autoroute.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connexions {
	 
    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/autoroute?verifyServerCertificate=false&useSSL=true","root","root");
        return connection ;
}
    public static void close() throws SQLException, ClassNotFoundException {
   	 getConnection().close();
    }
}