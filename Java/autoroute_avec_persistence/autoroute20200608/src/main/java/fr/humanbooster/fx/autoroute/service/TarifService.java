package fr.humanbooster.fx.autoroute.service;

import java.util.List;

import fr.humanbooster.fx.autoroute.business.Tarif;

public interface TarifService {

	Tarif ajouterTarif(Tarif tarif);

	Tarif recupererTarif(Long id);

	List<Tarif> recupererTarifs();

	Tarif supprimerTarif(Long id);

	Tarif updateTarif(Long id);
	
	

}
