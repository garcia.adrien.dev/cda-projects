package fr.humanbooster.fx.autoroute.service.impl;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.autoroute.business.Peage;
import fr.humanbooster.fx.autoroute.dao.PeageDao;
import fr.humanbooster.fx.autoroute.dao.impl.PeageDaoImpl;
import fr.humanbooster.fx.autoroute.service.PeageService;

public class PeageServiceImpl implements PeageService {
	private PeageDao peageDao = new PeageDaoImpl();

	@Override
	public Peage ajouterPeage(String nom) {
		Peage peage = new Peage(nom);
		try {
			peageDao.create(peage);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return peage;
	}

	@Override
	public Peage recupererPeage(Long id) {
		try {
			return peageDao.findOne(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Peage> recupererPeages() {
		try {
			return peageDao.findAll();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
