<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste des séries</title>
</head>
<body>
<h1>Bienvenue !</h1>
<h2>Liste des séries</h2>
<a href="?series_page=0&series_sort=nom">Trier par nom</a>
<a href="?series_page=0&series_sort=prixEnEuros,DESC">Trier par prix décroissant</a>
<ul>
<c:forEach items="${pageDeSeries.content}" var="serie">
	<li>${serie.nom} (${serie.prixEnEuros} &euro;) <a href="exportWord?ID_SERIE=${serie.id}">Export au format Word</a></li>
		<c:forEach items="${serie.saisons}" var="saison">
			${saison.nom}<br>
		</c:forEach>
</c:forEach>
</ul>
<h3>
Voilà : ${pageDeSeries.sort.by("nom")} <br>
Tri actuel : ${pageDeSeries.sort.iterator().next()}<br>
Propriété de tri ${pageDeSeries.sort.iterator().next().property}<br>
Direction de tri ${pageDeSeries.sort.iterator().next().direction}<br>
<c:if test="${!pageDeSeries.first}">
<a href="?series_page=0&series_sort=${pageDeSeries.sort.iterator().next().property},${pageDeSeries.sort.iterator().next().direction}&utilisateurs_page=${pageDUtilisateurs.number}&utilisateurs_sort=${pageDUtilisateurs.sort.iterator().next().property},${pageDUtilisateurs.sort.iterator().next().direction}">&#x23EE;</a>
<a href="?series_page=${pageDeSeries.number-1}&series_sort=${pageDeSeries.sort.iterator().next().property},${pageDeSeries.sort.iterator().next().direction}&utilisateurs_page=${pageDUtilisateurs.number}&utilisateurs_sort=${pageDUtilisateurs.sort.iterator().next().property},${pageDUtilisateurs.sort.iterator().next().direction}">&#x23EA;</a>
</c:if>
Page ${pageDeSeries.getNumber()+1}
<c:if test="${!pageDeSeries.last}">
<a href="?series_page=${pageDeSeries.number+1}&series_sort=${pageDeSeries.sort.iterator().next().property},${pageDeSeries.sort.iterator().next().direction}&utilisateurs_page=${pageDUtilisateurs.number}&utilisateurs_sort=${pageDUtilisateurs.sort.iterator().next().property},${pageDUtilisateurs.sort.iterator().next().direction}">&#x23E9;</a>
<a href="?series_page=${pageDeSeries.totalPages - 1}&series_sort=${pageDeSeries.sort.iterator().next().property},${pageDeSeries.sort.iterator().next().direction}&utilisateurs_page=${pageDUtilisateurs.number}&utilisateurs_sort=${pageDUtilisateurs.sort.iterator().next().property},${pageDUtilisateurs.sort.iterator().next().direction}">&#x23ED;</a>
</c:if>
</h3>

<h2>Liste des utilisateurs</h2>
<ul>
<c:forEach items="${pageDUtilisateurs.content}" var="utilisateur">
	<li>${utilisateur.email}</li>
</c:forEach>
</ul>
<h3>
<c:if test="${!pageDUtilisateurs.first}">
<a href="?utilisateurs_page=0&utilisateurs_sort=${pageDUtilisateurs.sort.iterator().next().property},${pageDUtilisateurs.sort.iterator().next().direction}&series_page=${pageDeSeries.number}&series_sort=${pageDeSeries.sort.iterator().next().property},${pageDeSeries.sort.iterator().next().direction}">&#x23EE;</a>
<a href="?utilisateurs_page=${pageDUtilisateurs.number-1}&utilisateurs_sort=${pageDUtilisateurs.sort.iterator().next().property},${pageDUtilisateurs.sort.iterator().next().direction}&series_page=${pageDeSeries.number}&series_sort=${pageDeSeries.sort.iterator().next().property},${pageDeSeries.sort.iterator().next().direction}">&#x23EA;</a>
</c:if>
Page ${pageDUtilisateurs.getNumber()+1}
<c:if test="${!pageDUtilisateurs.last}">
<a href="?utilisateurs_page=${pageDUtilisateurs.number+1}&utilisateurs_sort=${pageDUtilisateurs.sort.iterator().next().property},${pageDUtilisateurs.sort.iterator().next().direction}&series_page=${pageDeSeries.number}&series_sort=${pageDeSeries.sort.iterator().next().property},${pageDeSeries.sort.iterator().next().direction}">&#x23E9;</a>
<a href="?utilisateurs_page=${pageDUtilisateurs.totalPages - 1}&utilisateurs_sort=${pageDUtilisateurs.sort.iterator().next().property},${pageDUtilisateurs.sort.iterator().next().direction}&series_page=${pageDeSeries.number}&series_sort=${pageDeSeries.sort.iterator().next().property},${pageDeSeries.sort.iterator().next().direction}">&#x23ED;</a>
</c:if>
</h3>

<c:if test="${sessionScope.utilisateur eq null}">
<a href="connexion">Connexion</a><br>
<a href="inscription">Inscription</a><br>
</c:if>
<c:if test="${sessionScope.utilisateur ne null}">
<a href="deconnexion">Déconnexion</a>
</c:if>
</body>
</html>