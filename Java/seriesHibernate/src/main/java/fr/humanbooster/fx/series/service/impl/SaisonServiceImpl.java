package fr.humanbooster.fx.series.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.humanbooster.fx.series.business.Saison;
import fr.humanbooster.fx.series.business.Serie;
import fr.humanbooster.fx.series.dao.SaisonDao;
import fr.humanbooster.fx.series.service.SaisonService;

@Service
public class SaisonServiceImpl implements SaisonService {

	private SaisonDao saisonDao;

	public SaisonServiceImpl(SaisonDao saisonDao) {
		super();
		this.saisonDao = saisonDao;
	}

	@Override
	public Saison ajouterSaison(String nom, Serie serie) {
		Saison saison = new Saison();
		saison.setNom(nom);
		saison.setSerie(serie);
		saisonDao.save(saison);
		return saison;
	}

	@Override
	@Transactional(readOnly=true)
	public List<Saison> recupererSaisons() {
		return saisonDao.findAll();
	}

	@Override
	public boolean supprimerSaison(Long id) {
		Optional<Saison> saison = saisonDao.findById(id);
		
		if (!saison.isPresent()) {
			return false;
		}
		saisonDao.delete(saison.get());
		return true;
	}

	@Override
	public List<Saison> recupererSaisonsParSerie(Serie serie) {
		return saisonDao.findBySerie(serie);

	}

	@Override
	public Saison recupererSaison(Long id) {
		return saisonDao.findById(id).orElse(null);
	}
	

}