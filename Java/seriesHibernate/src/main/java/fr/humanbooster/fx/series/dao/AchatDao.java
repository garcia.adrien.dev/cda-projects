package fr.humanbooster.fx.series.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.series.business.Achat;

public interface AchatDao extends JpaRepository<Achat, Long> {

}
