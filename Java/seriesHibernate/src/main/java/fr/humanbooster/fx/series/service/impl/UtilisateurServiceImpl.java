package fr.humanbooster.fx.series.service.impl;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.humanbooster.fx.series.business.Utilisateur;
import fr.humanbooster.fx.series.dao.UtilisateurDao;
import fr.humanbooster.fx.series.service.UtilisateurService;

@Service
public class UtilisateurServiceImpl implements UtilisateurService {

	private UtilisateurDao utilisateurDao;

	public UtilisateurServiceImpl(UtilisateurDao utilisateurDao) {
		super();
		this.utilisateurDao = utilisateurDao;
	}

	@Override
	public Utilisateur ajouterUtilisateur(Utilisateur utilisateur) {
		return utilisateurDao.save(utilisateur);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Utilisateur> recupererUtilisateurs() {
		return utilisateurDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Page<Utilisateur> recupererUtilisateurs(Pageable pageable) {
		return utilisateurDao.findAll(pageable);
	}
	
	@Override
	public Utilisateur recupererUtilisateur(String email, String motDePasse) {
		return utilisateurDao.findFirstByEmailAndMotDePasse(email, motDePasse);
	}

}
