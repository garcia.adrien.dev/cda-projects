package fr.humanbooster.fx.series.business;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Serie {

	private static final long PRIX_MINIMUM = 10;
	private static final long PRIX_MAXIMUM = 100;
	
	@Id // indique la clé primaire de la table correspondante
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(length=100)
	private String nom;
	
	@Min(value=PRIX_MINIMUM, message="Le prix ne peut pas être inférieur à " + PRIX_MINIMUM + " euros")
	@Max(value=PRIX_MAXIMUM, message="Le prix ne peut pas être supérieur à " + PRIX_MAXIMUM + " euros")
	private float prixEnEuros;
	
	@JsonIgnore
	@OneToMany(mappedBy="serie", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	private List<Saison> saisons;
	
	public Serie() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public float getPrixEnEuros() {
		return prixEnEuros;
	}

	public void setPrixEnEuros(float prixEnEuros) {
		this.prixEnEuros = prixEnEuros;
	}

	public List<Saison> getSaisons() {
		return saisons;
	}

	public void setSaisons(List<Saison> saisons) {
		this.saisons = saisons;
	}

	@Override
	public String toString() {
		return "Serie [id=" + id + ", nom=" + nom + ", prixEnEuros=" + prixEnEuros + "]";
	}
	
	
}
