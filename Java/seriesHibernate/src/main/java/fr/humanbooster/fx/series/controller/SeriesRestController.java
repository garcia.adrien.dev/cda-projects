package fr.humanbooster.fx.series.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.humanbooster.fx.series.business.Episode;
import fr.humanbooster.fx.series.business.Saison;
import fr.humanbooster.fx.series.business.Serie;
import fr.humanbooster.fx.series.service.EpisodeService;
import fr.humanbooster.fx.series.service.SaisonService;
import fr.humanbooster.fx.series.service.SerieService;

@RestController
public class SeriesRestController {
	
	private SerieService serieService;
	private SaisonService saisonService;
	private EpisodeService episodeService;
	

	public SeriesRestController(SerieService serieService, SaisonService saisonService, EpisodeService episodeService) {
		super();
		this.serieService = serieService;
		this.saisonService = saisonService;
		this.episodeService = episodeService;
	}


	//A)
	@PostMapping("/serie/{nom}/{prix}")
	public Serie ajouterSerie(@PathVariable String nom, @PathVariable float prix) {
		Serie serie = serieService.ajouterSerie(nom, prix);
		return serie;
		
	}
	
	
	//B
	@PutMapping("/modserie/{nom}/{prix}/{id}")
	public Serie modifSerie(@PathVariable String nom, @PathVariable float prix,  @PathVariable Long id) {
		
		Serie serie = serieService.recupererSerie(id) ;
		serie.setNom(nom);
		serie.setPrixEnEuros(prix);
		serieService.modifierSerie(serie);
		return serie;	
	}
	
	//C
	@DeleteMapping("/supserie/{id}")
	public boolean supSerie(@PathVariable Long id) {
	
		return (serieService.supprimerSerie(id));
		
	}
	
	//E
	@GetMapping("/recupsaison/{id}/saisons")
	public List<Saison> recupSaison(@PathVariable Long id)
	{
		return (saisonService.recupererSaisonsParSerie(serieService.recupererSerie(id)));
		
	}
	
	//F
	@PostMapping("/ajoutsaison/{id}/{nom}")
	public Saison ajouterSaison(@PathVariable Long id, @PathVariable String nom) {
		
		return (saisonService.ajouterSaison(nom, serieService.recupererSerie(id)));
	}
	
	//G
	@PostMapping("/ajoutepisode/{id}/{nom}/{duree}")
	public Episode ajouterEpisode(@PathVariable Long id, @PathVariable String nom,  @PathVariable int duree) {
			
		return (episodeService.ajouterEpisode(nom, duree, saisonService.recupererSaison(id)));
		
	}
	
	//H
	@GetMapping("/recupserie/seriemax")
	public Serie recupererSerieMax() {

		   List<Serie> series = serieService.recupererSeries();
	        if (series.isEmpty()) {
	            return null;
	        }
	        Serie serieM = null;
	        for (Serie serie : series) {
	            if (serieM == null || serie.getPrixEnEuros() > serieM.getPrixEnEuros()) {
	                serieM = serie;
	            }
	        }
	        return serieM;
	    }
	
	@GetMapping("/recupserie/serielongue")
	public Serie recupererSerieLongue() {
		 List<Serie> series = serieService.recupererSeries();
	        if (series.isEmpty()) {
	            return null;
	        }
	       Serie serieL = null;
	       for (Serie serie : series) {
	    	   List<Episode> episodes = episodeService.trouverParSerie(serie);
	    	   for(Episode episode : episodes) {
	    		  
	    	   }
	       }
		return serieL;
	}
	}

   
	
