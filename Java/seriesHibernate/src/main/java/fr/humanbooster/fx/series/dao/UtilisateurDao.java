package fr.humanbooster.fx.series.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.series.business.Utilisateur;

public interface UtilisateurDao extends JpaRepository<Utilisateur, Long> {

	Utilisateur findFirstByEmailAndMotDePasse(String email, String motDePasse);
	
}
