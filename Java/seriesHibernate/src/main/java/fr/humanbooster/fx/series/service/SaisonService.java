package fr.humanbooster.fx.series.service;

import java.util.List;

import fr.humanbooster.fx.series.business.Saison;
import fr.humanbooster.fx.series.business.Serie;

public interface SaisonService {

	Saison ajouterSaison(String nom, Serie serie);
	
	List<Saison> recupererSaisonsParSerie(Serie serie);
	
	List<Saison> recupererSaisons();
	
	Saison recupererSaison(Long id);
	
	boolean supprimerSaison(Long id);
}
