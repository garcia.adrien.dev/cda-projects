package fr.humanbooster.fx.series.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.series.business.Interet;

public interface InteretDao extends JpaRepository<Interet, Long> {

}
