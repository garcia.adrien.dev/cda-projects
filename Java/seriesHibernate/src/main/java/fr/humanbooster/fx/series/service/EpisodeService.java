package fr.humanbooster.fx.series.service;

import java.util.List;

import fr.humanbooster.fx.series.business.Episode;
import fr.humanbooster.fx.series.business.Saison;
import fr.humanbooster.fx.series.business.Serie;

public interface EpisodeService {
	
	Episode ajouterEpisode(String nom, int dureeEnMinutes, Saison saison);
	
	List<Episode> trouverParSerie(Serie serie);

}
