package fr.humanbooster.fx.series.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.humanbooster.fx.series.business.Episode;
import fr.humanbooster.fx.series.business.Saison;
import fr.humanbooster.fx.series.business.Serie;
import fr.humanbooster.fx.series.dao.EpisodeDao;
import fr.humanbooster.fx.series.service.EpisodeService;

@Service
public class EpisodeServiceImpl implements EpisodeService {
	
	EpisodeDao episodeDao;
	
	

	public EpisodeServiceImpl(EpisodeDao episodeDao) {
		super();
		this.episodeDao = episodeDao;
	}



	@Override
	public Episode ajouterEpisode(String nom, int dureeEnMinutes, Saison saison) {
		
			Episode episode= new Episode();
			episode.setDureeEnMinutes(dureeEnMinutes);
			episode.setNom(nom);
			episode.setSaison(saison);
			episodeDao.save(episode);
			return episode;
		}



	@Override
	public List<Episode> trouverParSerie(Serie serie) {
		return (episodeDao.findAllBySaisonSerie(serie));
	}


}
