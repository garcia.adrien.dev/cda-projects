package fr.humanbooster.Slam2Vile.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

import fr.humanbooster.Slam2Vile.business.Ville;
import fr.humanbooster.Slam2Vile.service.VilleService;

@Service
public class VilleServiceImpl implements VilleService {

	List<Ville> villes = new ArrayList<Ville>();
	
	   public VilleServiceImpl() {
	        if (villes.isEmpty()) {
	            recupererVillesEnCsv();
	            trierVilles();
	        }
	   }

	@Override
	public void recupererVillesEnCsv() {
		BufferedReader br = null;
		String cvsSplitBy = ";";
		String line = "";

		try {
			URL u = new URL("https://www.clelia.fr/villes.csv");
			br = new BufferedReader(new InputStreamReader(u.openStream()));

			br.readLine();

			while ((line = br.readLine()) != null) {
				String[] info = line.split(cvsSplitBy);
				Ville ville = new Ville(info[0], info[1]);
				villes.add(ville);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("Villes chargées: " + villes.size());
	}

	@Override
	public void trierVilles() {
		Collections.sort(villes);
	}

	@Override
	public List<Ville> recupererVille(String nomSaisi) {
		List<Ville> cpTrouve = new ArrayList<Ville>();
		if (nomSaisi == null)
			return cpTrouve;
		Iterator<Ville> itr = villes.iterator();
		while (itr.hasNext()) {
			Ville ville = (Ville) itr.next();
			if (ville.getNom() != null && ville.getNom().toUpperCase().startsWith(nomSaisi.toUpperCase())) {
				cpTrouve.add(ville);
			}
		}
		if (cpTrouve.size() == 0) {
			return null;
		}
		return cpTrouve;
	}

}
