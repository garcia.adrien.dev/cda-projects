package fr.humanbooster.Slam2Vile.service;

import java.util.List;

import fr.humanbooster.Slam2Vile.business.Ville;

public interface VilleService {

	void recupererVillesEnCsv();

	void trierVilles();

	List<Ville> recupererVille(String nomSaisi);

}