package fr.humanbooster.Slam2Vile.business;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Ville implements Comparable<Ville>
{
   private int id;
   private String codePostal;
   private String nom;
   private static int currentId = 0;

   public Ville(String codePostal, String nom)
   {
      this.codePostal = codePostal;
      this.nom = nom;
      this.id=currentId;

      currentId++;
   }
   
   @JsonProperty(value="id")
   public int getId()
   {
      return id;
   }

   @JsonIgnore()
   public String getCodePostal()
   {
      return codePostal;
   }
   
   @JsonIgnore()
   public String getNom()
   {
      return nom;
   }
   
   @JsonProperty(value="label")
   public String getNomEtCodePostal()
   {
	  String chaine = "";
	  chaine = nom + " (";
	  if (codePostal.length() == 4) chaine += "0";
	  chaine += codePostal + ")";
      return chaine;
   }

   public int compareTo(Ville autreVille)
   {
      return getNom().compareTo(autreVille.getNom());
   }

}