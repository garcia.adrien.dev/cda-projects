package fr.humanbooster.Slam2Vile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Slam2VileApplication {

	public static void main(String[] args) {
		SpringApplication.run(Slam2VileApplication.class, args);
	}

}
