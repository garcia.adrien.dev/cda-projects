package fr.humanbooster.Slam2Vile.controller;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import fr.humanbooster.Slam2Vile.business.VillesCorrespondantes;
import fr.humanbooster.Slam2Vile.service.VilleService;

@RestController
public class VilleController {
	
	private static final Logger logger = LogManager.getLogger(Logger.class.getName());
	
	VilleService villeService;

	public VilleController(VilleService villeService) {
		super();
		this.villeService = villeService;
	}
	
	@PostConstruct
    public void init() {
        
        villeService.recupererVillesEnCsv();
    }
	
	@RequestMapping(value="{index, /}")
	public ModelAndView acceuil() {
		ModelAndView mav = new ModelAndView("index");
		
		return mav;
	}
	
	@GetMapping("/RechercheVilleJson")
    public VillesCorrespondantes recupererVilles(@RequestParam("name") String nomSaisi,  @RequestParam("start") , @RequestParam("count") ) {
		return null;}

}
