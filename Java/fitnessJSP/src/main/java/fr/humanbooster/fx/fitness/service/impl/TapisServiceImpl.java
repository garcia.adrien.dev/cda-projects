package fr.humanbooster.fx.fitness.service.impl;

import java.sql.SQLException;
import java.util.List;


import fr.humanbooster.fx.fitness.business.Tapis;
import fr.humanbooster.fx.fitness.dao.TapisDao;
import fr.humanbooster.fx.fitness.dao.impl.TapisDaoImpl;
import fr.humanbooster.fx.fitness.service.TapisService;



public class TapisServiceImpl implements TapisService {

	private TapisDao tapisDao  = new TapisDaoImpl();
	
	@Override
	public Tapis ajouterTapis(String numeroSerie,  String designation) {
		Tapis tapis = new Tapis(numeroSerie, designation);
		try {
			tapisDao.create(tapis);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return tapis;
	}
	
	public Tapis ajouterTapis(Long id, String numeroSerie,  String designation) {
		Tapis tapis = new Tapis(id, numeroSerie, designation);
		try {
			tapisDao.create(tapis);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return tapis;
	}

	@Override
	public Tapis recupererTapisParId(Long id) {
		try {
			tapisDao.findOne(id);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Tapis> recupererLisTapis() {
		try {
			return tapisDao.findAll();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	
}
