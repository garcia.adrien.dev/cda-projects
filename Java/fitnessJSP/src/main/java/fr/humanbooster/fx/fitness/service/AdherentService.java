package fr.humanbooster.fx.fitness.service;

import java.util.List;

import fr.humanbooster.fx.fitness.business.Adherent;


public interface AdherentService {

	Adherent ajouterAdherent(String nom, String prenom, String email, String motDePasse);
	Adherent recupererAdherentParId(Long id);
	List<Adherent> recupererAdherents();
	Adherent recupererAdherentsParEmailEtMdp(String email, String motDePasse);


}
