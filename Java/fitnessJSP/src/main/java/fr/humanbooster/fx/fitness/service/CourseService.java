package fr.humanbooster.fx.fitness.service;


import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.fitness.business.Adherent;
import fr.humanbooster.fx.fitness.business.Course;
import fr.humanbooster.fx.fitness.business.Tapis;

public interface CourseService {

	List<Course> recupererCourses();

	Course recupererCourseParId(Long id);
	Course ajouterCourse(float calories, Date dateHeureDebut, int dureeEnMinutes, int distanceEnMetres, Adherent adherent, Tapis tapis);

	List<Course> recupererParAdherentEtEntreDateHeureDebut(Adherent adherent, Date dateDebut, Date dateFin);
	Course updateCourse(Course course);
	
}
