package fr.humanbooster.fx.fitness.service;

import java.util.List;

import fr.humanbooster.fx.fitness.business.Tapis;

public interface TapisService {

	Tapis ajouterTapis(String numeroSerie, String designation);
	
	Tapis ajouterTapis(Long id, String numeroSerie, String designation);

	Tapis recupererTapisParId(Long id);

	List<Tapis> recupererLisTapis();


}
