package fr.humanbooster.fx.fitness.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.fitness.business.Adherent;
import fr.humanbooster.fx.fitness.service.AdherentService;
import fr.humanbooster.fx.fitness.service.impl.AdherentServiceImpl;

/**
 * Servlet implementation class IndexServlet
 */
@WebServlet(urlPatterns="/index", loadOnStartup=1)
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private AdherentService adherentService = null;
	
	public IndexServlet() {
		super();
		adherentService = new AdherentServiceImpl();
	
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.getRequestDispatcher("WEB-INF/index.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("EMAIL");
		String motDePasse = request.getParameter("MOT_DE_PASSE");
		
		// On essaie de récupérer un joueur avec ces données
		Adherent adherent = adherentService.recupererAdherentsParEmailEtMdp(email, motDePasse);
		if (adherent != null) {
			// Le joueur a saisi le bon email + mdp
			
			// Ajout d'une partie et mise en session de cette partie
			request.getSession().setAttribute("adherent", adherent);
			// On passe le relai à la servlet QuestionServlet
			request.getRequestDispatcher("WEB-INF/acceuil.jsp").include(request, response);
		} else {
			request.setAttribute("erreur", "email et/ou mot de passe incorrect");
			doGet(request, response);
		}
	}

}
