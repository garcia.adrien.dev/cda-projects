package fr.humanbooster.fx.fitness.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.fitness.business.Course;
import fr.humanbooster.fx.fitness.service.CourseService;
import fr.humanbooster.fx.fitness.service.impl.CourseServiceImpl;
import fr.humanbooster.fx.fitness.util.ComparatorSurDistance;
import fr.humanbooster.fx.fitness.util.ComparatorSurDuree;


/**
 * Servlet implementation class AcceuilServlet
 */
@WebServlet (urlPatterns = "/acceuil", loadOnStartup=1)
public class AcceuilServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CourseService courseService;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AcceuilServlet() {
        super();
        courseService = new CourseServiceImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Course> courses = courseService.recupererCourses();
		int idTri = 0;
		
		if (request.getParameter("ID_TRI")!=null) {
			idTri = Integer.parseInt(request.getParameter("ID_TRI"));
		}
		if (idTri==1) {
			Collections.sort(courses, new ComparatorSurDistance());
		}
		else if (idTri==2){
			Collections.sort(courses, new ComparatorSurDuree());
		}
		request.setAttribute("idTri", idTri);
		request.setAttribute("courses", courses);
		request.getRequestDispatcher("WEB-INF/acceuil.jsp").forward(request, response);
		
	}


}
