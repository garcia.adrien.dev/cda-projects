package fr.humanbooster.fx.fitness.service.impl;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.fitness.business.Adherent;
import fr.humanbooster.fx.fitness.dao.AdherentDao;
import fr.humanbooster.fx.fitness.dao.impl.AdherentDaoImpl;
import fr.humanbooster.fx.fitness.service.AdherentService;

public class AdherentServiceImpl implements AdherentService {
	
	private AdherentDao adherentDao  = new AdherentDaoImpl();
	
	public AdherentServiceImpl() {
        try {
			if(adherentDao.findAll().isEmpty()) {
			    for (int i = 1; i <=2; i++) {
			        ajouterAdherent("adherent" + i, "adherent" + i, "adherent"+i+"@hb.fr", "adherent"+i);
			    }
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
	
	public Adherent ajouterAdherent(String nom, String prenom, String email, String motDePasse) {
		Adherent adherent = new Adherent(nom, prenom, email, motDePasse);
		try {
			adherentDao.create(adherent);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return adherent;
	}

	@Override
	public Adherent recupererAdherentParId(Long id) {
		try {
			adherentDao.findOne(id);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public List<Adherent> recupererAdherents() {
		try {
			return adherentDao.findAll();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Adherent recupererAdherentsParEmailEtMdp(String email, String motDePasse) {
		try {
			return adherentDao.findByEmailAndMotDePasse(email, motDePasse);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
