package fr.humanbooster.fx.fitness.service.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.fitness.business.Adherent;
import fr.humanbooster.fx.fitness.business.Course;
import fr.humanbooster.fx.fitness.business.Tapis;
import fr.humanbooster.fx.fitness.dao.CourseDao;
import fr.humanbooster.fx.fitness.dao.impl.CourseDaoImpl;
import fr.humanbooster.fx.fitness.service.CourseService;

public class CourseServiceImpl implements CourseService {
	
	private CourseDao courseDao  = new CourseDaoImpl();
	
	public Course ajouterCourse(float calories, Date dateHeureDebut, int dureeEnMinutes, 
			int distanceEnMetres, Adherent adherent, Tapis tapis) {
		Course course = new Course(calories, dateHeureDebut, dureeEnMinutes, distanceEnMetres, adherent, tapis);
		try {
			courseDao.create(course);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return course;
	}

	@Override
	public Course recupererCourseParId(Long id) {
		try {
			courseDao.findOne(id);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Course> recupererCourses() {
		try {
			return courseDao.findAll();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	@Override
	public List<Course> recupererParAdherentEtEntreDateHeureDebut(Adherent adherent, Date dateDebut, Date dateFin){
		try {
			return courseDao.findByAdherentAndDateHeureDebutBetween(adherent, dateDebut, dateFin);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Course updateCourse(Course course) {
		try {
			courseDao.update(course);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
