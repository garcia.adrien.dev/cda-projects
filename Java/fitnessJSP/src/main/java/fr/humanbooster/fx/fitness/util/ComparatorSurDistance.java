package fr.humanbooster.fx.fitness.util;

import java.util.Comparator;

import fr.humanbooster.fx.fitness.business.Course;

public class ComparatorSurDistance implements Comparator<Course>{

    @Override
    public int compare(Course course1, Course course2) {
        if (course1.getDistanceEnMetres() == course2.getDistanceEnMetres()){
            return 0;
        }
        else if (course1.getDistanceEnMetres() > course2.getDistanceEnMetres()) {
            return -1;
        }
        else {
            return 1;
        }
    }
}