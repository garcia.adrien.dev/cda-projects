<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>fitness</title>
</head>
<body>
	<jsp:include page="entete.jsp" />
	<h1>Vos courses</h1>

	<form action="courses" method="get">
		Course effectuer entre le<input type="date" id="start" name="trip-start"> et le 
		<input type="date" id="start" name="trip-start"><input type="submit" value="Filtrer">

		<p></p>
		<table>
			<tr>
				<td>Date et heure<c:if test="${idTri ne 0}"><a href="courses?ID_TRI=0"></a></c:if></td>
				<td>Distance (m)<c:if test="${idTri ne 1}"><a href="courses?ID_TRI=1"></a></c:if></td>
				<td>Durée<c:if test="${idTri ne 2}"><a href="courses?ID_TRI=2"></a></c:if></td>
				<td>Calories</td>
				<td>Action</td>
			</tr>
			<c:forEach var="course" items="${courses}">
				<tr>
					<td>${course.dateHeureDebut}</td>
					<td>${course.distanceEnMetres}</td>
					<td>${course.dureeEnMinutes}</td>
					<td>${course.calories}</td>
					<td><a href="">Modifier</a></td>
				</tr>
			</c:forEach>
		</table>
	</form>
	<a href="course.jsp">Ajouter une course</a>
</body>
</html>
