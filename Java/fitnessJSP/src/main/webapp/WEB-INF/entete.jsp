<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:choose>
	<c:when test="${sessionScope.utilisateur ne null }">
		<a href="deconnexion">Deconnexion</a>
		<br>
		Bienvenue ${sessionScope.utilisateur.prenom} ! ,${sessionScope.utilisateur.nom} !
	</c:when>
</c:choose>