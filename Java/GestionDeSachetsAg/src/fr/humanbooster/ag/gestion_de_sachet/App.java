package fr.humanbooster.ag.gestion_de_sachet;

import java.util.Date;
import java.util.Scanner;

import fr.humanbooster.ag.gestion_de_sachet.metier.Fournisseur;
import fr.humanbooster.ag.gestion_de_sachet.metier.TypeDeGraine;
import fr.humanbooster.ag.gestion_de_sachet.service.FamilleService;
import fr.humanbooster.ag.gestion_de_sachet.service.FournisseurService;
import fr.humanbooster.ag.gestion_de_sachet.service.SachetService;
import fr.humanbooster.ag.gestion_de_sachet.service.TypeDeGraineService;
import fr.humanbooster.ag.gestion_de_sachet.service.impl.FamilleServiceImpl;
import fr.humanbooster.ag.gestion_de_sachet.service.impl.FournisseurServiceImpl;
import fr.humanbooster.ag.gestion_de_sachet.service.impl.SachetServiceImpl;
import fr.humanbooster.ag.gestion_de_sachet.service.impl.TypeDeGraineServiceImpl;

public class App {

	private static FamilleService familleService = new FamilleServiceImpl();
	private static FournisseurService fournisseurService = new FournisseurServiceImpl();
	private static SachetService sachetService = new SachetServiceImpl();
	private static TypeDeGraineService typeDeGraineService = new TypeDeGraineServiceImpl();
	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		creerProduit();

	}

	public static void creerProduit() {

		fournisseurService.ajouterFournisseur("mainverte");
		fournisseurService.ajouterFournisseur("bonjardinier");

		familleService.ajouterFamille("legume");
		familleService.ajouterFamille("haricot");

		typeDeGraineService.ajouterTypeDeGraine("haricot nain extra fin", familleService.recupererFamille(2L), null,
				null, null, null, null);
		typeDeGraineService.ajouterTypeDeGraine("courgette verte noire maraichere", familleService.recupererFamille(1L),
				null, null, null, null, null);
		typeDeGraineService.ajouterTypeDeGraine("oignon jaune paille des vertue", familleService.recupererFamille(1L),
				null, null, null, null, null);
		typeDeGraineService.ajouterTypeDeGraine("carrote nantaise", familleService.recupererFamille(1L), null, null,
				null, null, null);

		System.out.println(typeDeGraineService.recupererTypeDeGraines());
	}

	public void creerSachet() {

		Fournisseur fournisseur = new Fournisseur();
		TypeDeGraine typeDeGraine = new TypeDeGraine();
		long fournisseurId = 0L;
		long typeDeGraineId = 0L;
		float poidEnGramme = 0F;

		while (fournisseurId < 1 || fournisseurId > 2) {
			System.out.println("Veuiller saisir un fournisseur");
			System.out.println("1 : legume");
			System.out.println("2 : haricot");
			fournisseur.setId.add = scanner.nextLong(); // Je voulais set l'id diurectement dans l'obj mais j'ai oublier
			// comment on fesait
		}

		while (typeDeGraineId < 1 || typeDeGraineId > 4) {
			System.out.println("Veuiller saisir un type de graine");
			System.out.println("1 : haricot nain extra fin");
			System.out.println("2 : courgette verte noire maraichere");
			System.out.println("3 : oignon jaune paille des vertue");
			System.out.println("4: carrote nantaise");
			typeDeGraineId = scanner.nextLong();
		}

		System.out.println("Veuiller saisir un poid en grammes");
		poidEnGramme = scanner.nextFloat();

		System.out.println("Veuiller saisir un prix en �");

		sachetService.ajouterSachet(Fournisseur, TypeDeGraine, new Date(), poidEnGramme, scanner.nextFloat());
	}
}
