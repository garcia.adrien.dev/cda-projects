package fr.humanbooster.ag.gestion_de_sachet.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.ag.gestion_de_sachet.metier.Fournisseur;
import fr.humanbooster.ag.gestion_de_sachet.service.FournisseurService;

public class FournisseurServiceImpl implements FournisseurService {

	private List<Fournisseur> fournisseurs = new ArrayList<>();

	@Override
	public Fournisseur ajouterFournisseur(String nom) {
		Fournisseur fournisseur = new Fournisseur(nom);
		fournisseurs.add(fournisseur);
		return fournisseur;
	}

	@Override
	public List<Fournisseur> recupererFournisseurs() {
		return fournisseurs;
	}

	@Override
	public Fournisseur recupererFournisseur(Long id) {
		for (Fournisseur fournisseur : fournisseurs) {
			if (id.equals(fournisseur.getId())) {
				return fournisseur;
			}
		}
		return null;
	}

	@Override
	public boolean supprimerFournisseur(Long id) {
		Fournisseur fournisseur = recupererFournisseur(id);
		if (fournisseur != null) {
			fournisseurs.remove(fournisseur);
			return true;
		}
		return false;
	}

}