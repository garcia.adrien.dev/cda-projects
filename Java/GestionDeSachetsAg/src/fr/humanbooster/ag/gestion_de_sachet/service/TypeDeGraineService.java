package fr.humanbooster.ag.gestion_de_sachet.service;

import java.util.List;

import fr.humanbooster.ag.gestion_de_sachet.metier.Famille;
import fr.humanbooster.ag.gestion_de_sachet.metier.TypeDeGraine;

public interface TypeDeGraineService {

	TypeDeGraine ajouterTypeDeGraine(String nom, Famille famille, Integer semaineDePlantationMin,
			Integer semaineDePlantationMax, Float espacementEntrePiedEnCentimetre,
			Float espacementEntreLigneEnCentimetre, String conseil);

	List<TypeDeGraine> recupererTypeDeGraines();

	TypeDeGraine recupererTypeDeGraine(Long id);

	boolean supprimerTypeDeGraine(Long id);

}
