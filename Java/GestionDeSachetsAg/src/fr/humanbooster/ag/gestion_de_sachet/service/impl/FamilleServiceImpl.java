package fr.humanbooster.ag.gestion_de_sachet.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.ag.gestion_de_sachet.metier.Famille;
import fr.humanbooster.ag.gestion_de_sachet.service.FamilleService;

public class FamilleServiceImpl implements FamilleService {

	private List<Famille> familles = new ArrayList<>();

	@Override
	public Famille ajouterFamille(String nom) {
		Famille famille = new Famille(nom);
		familles.add(famille);
		return famille;
	}

	@Override
	public List<Famille> recupererFamilles() {
		return familles;
	}

	@Override
	public Famille recupererFamille(Long id) {
		for (Famille famille : familles) {
			if (id.equals(famille.getId())) {
				return famille;
			}
		}
		return null;
	}

	@Override
	public boolean supprimerFamille(Long id) {
		Famille famille = recupererFamille(id);
		if (famille != null) {
			familles.remove(famille);
			return true;
		}
		return false;
	}

}
