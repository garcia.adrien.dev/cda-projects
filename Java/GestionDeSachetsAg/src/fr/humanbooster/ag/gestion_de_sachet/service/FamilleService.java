package fr.humanbooster.ag.gestion_de_sachet.service;

import java.util.List;

import fr.humanbooster.ag.gestion_de_sachet.metier.Famille;

public interface FamilleService {

	Famille ajouterFamille(String nom);

	List<Famille> recupererFamilles();

	Famille recupererFamille(Long id);

	boolean supprimerFamille(Long id);

}
