package fr.humanbooster.ag.gestion_de_sachet.service;

import java.util.Date;
import java.util.List;

import fr.humanbooster.ag.gestion_de_sachet.metier.Fournisseur;
import fr.humanbooster.ag.gestion_de_sachet.metier.Sachet;
import fr.humanbooster.ag.gestion_de_sachet.metier.TypeDeGraine;

public interface SachetService {

	Sachet ajouterSachet(Fournisseur fournisseur, TypeDeGraine typeDeGraine,
			Date dateDeCreation, float poidsEnGrammes, float prixEnEuros);

	List<Sachet> recupererSachets();

	Sachet recupererSachet(Long id);

	boolean supprimerSachet(Long id);

}