package fr.humanbooster.ag.gestion_de_sachet.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.humanbooster.ag.gestion_de_sachet.metier.Fournisseur;
import fr.humanbooster.ag.gestion_de_sachet.metier.Sachet;
import fr.humanbooster.ag.gestion_de_sachet.metier.TypeDeGraine;
import fr.humanbooster.ag.gestion_de_sachet.service.SachetService;

public class SachetServiceImpl implements SachetService {

	private List<Sachet> sachets = new ArrayList<>();

	@Override
	public Sachet ajouterSachet(Fournisseur fournisseur, TypeDeGraine typeDeGraine, Date dateDeCreation, float poidsEnGrammes, float prixEnEuros) {
		Sachet sachet = new Sachet(fournisseur, typeDeGraine, dateDeCreation, poidsEnGrammes, prixEnEuros);
		sachets.add(sachet);
		return sachet;
	}

	@Override
	public List<Sachet> recupererSachets() {
		return sachets;
	}

	@Override
	public Sachet recupererSachet(Long id) {
		for (Sachet sachet : sachets) {
			if (id.equals(sachet.getId())) {
				return sachet;
			}
		}
		return null;
	}

	@Override
	public boolean supprimerSachet(Long id) {
		Sachet sachet = recupererSachet(id);
		if (sachet != null) {
			sachets.remove(sachet);
			return true;
		}
		return false;
	}

}