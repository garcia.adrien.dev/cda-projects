package fr.humanbooster.ag.gestion_de_sachet.service;

import java.util.List;

import fr.humanbooster.ag.gestion_de_sachet.metier.Fournisseur;

public interface FournisseurService {

	Fournisseur ajouterFournisseur(String nom);

	List<Fournisseur> recupererFournisseurs();

    Fournisseur recupererFournisseur(Long id);

	boolean supprimerFournisseur(Long id);

}