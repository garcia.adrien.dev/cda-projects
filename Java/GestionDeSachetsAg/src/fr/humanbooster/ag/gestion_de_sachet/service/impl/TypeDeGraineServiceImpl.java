package fr.humanbooster.ag.gestion_de_sachet.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.ag.gestion_de_sachet.metier.Famille;
import fr.humanbooster.ag.gestion_de_sachet.metier.TypeDeGraine;
import fr.humanbooster.ag.gestion_de_sachet.service.TypeDeGraineService;

public class TypeDeGraineServiceImpl implements TypeDeGraineService {

	private List<TypeDeGraine> typeDeGraines = new ArrayList<>();

	@Override
	public TypeDeGraine ajouterTypeDeGraine(String nom, Famille famille, Integer semaineDePlantationMin,
			Integer semaineDePlantationMax, Float espacementEntrePiedEnCentimetre,
			Float espacementEntreLigneEnCentimetre, String conseil) {
		TypeDeGraine typeDeGraine = new TypeDeGraine(nom, famille, semaineDePlantationMin, semaineDePlantationMax,
				espacementEntrePiedEnCentimetre, espacementEntreLigneEnCentimetre, conseil);
		typeDeGraines.add(typeDeGraine);
		return typeDeGraine;
	}

	@Override
	public List<TypeDeGraine> recupererTypeDeGraines() {
		return typeDeGraines;
	}

	@Override
	public TypeDeGraine recupererTypeDeGraine(Long id) {
		for (TypeDeGraine typeDeGraine : typeDeGraines) {
			if (id.equals(typeDeGraine.getId())) {
				return typeDeGraine;
			}
		}
		return null;
	}

	@Override
	public boolean supprimerTypeDeGraine(Long id) {
		TypeDeGraine typeDeGraine = recupererTypeDeGraine(id);
		if (typeDeGraine != null) {
			typeDeGraines.remove(typeDeGraine);
			return true;
		}
		return false;
	}

}
