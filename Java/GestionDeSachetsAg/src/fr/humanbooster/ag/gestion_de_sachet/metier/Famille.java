package fr.humanbooster.ag.gestion_de_sachet.metier;

import java.util.List;

public class Famille {

	private List<TypeDeGraine> typeDeGraines;
	private Long id;
	private String nom;
	private static Long compteur = 0L;

	public Famille() {
		id = ++compteur;
	}

	public Famille(String nom) {
		this();
		this.nom = nom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<TypeDeGraine> getTypeDeGraines() {
		return typeDeGraines;
	}

	public void setTypeDeGraines(List<TypeDeGraine> typeDeGraines) {
		this.typeDeGraines = typeDeGraines;
	}

	@Override
	public String toString() {
		return "Famille [typeDeGraines=" + typeDeGraines + ", id=" + id + ", nom=" + nom + "]";
	}

}
