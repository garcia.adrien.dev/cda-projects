package fr.humanbooster.ag.gestion_de_sachet.metier;

import java.util.List;

public class TypeDeGraine {

	private List<Sachet> sachets;
	private Famille famille;
	private Long id;
	private String nom;
	private Integer semaineDePlantationMin;
	private Integer semaineDePlantationMax;
	private Float espacementEntrePiedEnCentimetre;
	private Float espacementEntreLigneEnCentimetre;
	private String conseil;
	private static Long compteur = 0L;

	public TypeDeGraine() {
		id = ++compteur;
	}

	public TypeDeGraine(String nom, Famille famille, Integer semaineDePlantationMin, Integer semaineDePlantationMax,
			Float espacementEntrePiedEnCentimetre, Float espacementEntreLigneEnCentimetre, String conseil) {
		this();
		this.nom = nom;
		this.famille = famille;
		this.semaineDePlantationMin = semaineDePlantationMin;
		this.semaineDePlantationMax = semaineDePlantationMax;
		this.espacementEntrePiedEnCentimetre = espacementEntrePiedEnCentimetre;
		this.espacementEntreLigneEnCentimetre = espacementEntreLigneEnCentimetre;
		this.conseil = conseil;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Integer getSemaineDePlantationMin() {
		return semaineDePlantationMin;
	}

	public void setSemaineDePlantationMin(Integer semaineDePlantationMin) {
		this.semaineDePlantationMin = semaineDePlantationMin;
	}

	public Integer getSemaineDePlantationMax() {
		return semaineDePlantationMax;
	}

	public void setSemaineDePlantationMax(Integer semaineDePlantationMax) {
		this.semaineDePlantationMax = semaineDePlantationMax;
	}

	public Float getEspacementEntrePiedEnCentimetre() {
		return espacementEntrePiedEnCentimetre;
	}

	public void setEspacementEntrePiedEnCentimetre(Float espacementEntrePiedEnCentimetre) {
		this.espacementEntrePiedEnCentimetre = espacementEntrePiedEnCentimetre;
	}

	public Float getEspacementEntreLigneEnCentimetre() {
		return espacementEntreLigneEnCentimetre;
	}

	public void setEspacementEntreLigneEnCentimetre(Float espacementEntreLigneEnCentimetre) {
		this.espacementEntreLigneEnCentimetre = espacementEntreLigneEnCentimetre;
	}

	public String getConseil() {
		return conseil;
	}

	public void setConseil(String conseil) {
		this.conseil = conseil;
	}

	public List<Sachet> getSachets() {
		return sachets;
	}

	public void setSachets(List<Sachet> sachets) {
		this.sachets = sachets;
	}

	public Famille getFamille() {
		return famille;
	}

	public void setFamille(Famille famille) {
		this.famille = famille;
	}

	@Override
	public String toString() {
		return "TypeDeGraine [sachets=" + sachets + ", famille=" + famille + ", id=" + id + ", nom=" + nom
				+ ", semaineDePlantationMin=" + semaineDePlantationMin + ", semaineDePlantationMax="
				+ semaineDePlantationMax + ", espacementEntrePiedEnCentimetre=" + espacementEntrePiedEnCentimetre
				+ ", espacementEntreLigneEnCentimetre=" + espacementEntreLigneEnCentimetre + ", conseil=" + conseil
				+ "]";
	}

}
