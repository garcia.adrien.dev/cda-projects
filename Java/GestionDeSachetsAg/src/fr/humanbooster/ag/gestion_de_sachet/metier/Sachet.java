package fr.humanbooster.ag.gestion_de_sachet.metier;

import java.util.Date;

public class Sachet {

	private Fournisseur fournisseur;
	private TypeDeGraine typeDeGraine;
	private Long id;
	private Date dateDeCreation;
	private float poidsEnGrammes;
	private float prixEnEuros;
	private static Long compteur = 0L;

	public Sachet() {
		id = ++compteur;
	}

	public Sachet(Fournisseur fournisseur, TypeDeGraine typeDeGraine, 
			Date dateDeCreation, float poidsEnGrammes, float prixEnEuros) {
		this();
		this.dateDeCreation = dateDeCreation;
		this.poidsEnGrammes = poidsEnGrammes;
		this.prixEnEuros = prixEnEuros;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateDeCreation() {
		return dateDeCreation;
	}

	public void setDateDeCreation(Date dateDeCreation) {
		this.dateDeCreation = dateDeCreation;
	}

	public float getPoidsEnGrammes() {
		return poidsEnGrammes;
	}

	public void setPoidsEnGrammes(float poidsEnGrammes) {
		this.poidsEnGrammes = poidsEnGrammes;
	}

	public float getPrixEnEuros() {
		return prixEnEuros;
	}

	public void setPrixEnEuros(float prixEnEuros) {
		this.prixEnEuros = prixEnEuros;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public TypeDeGraine getTypeDeGraine() {
		return typeDeGraine;
	}

	public void setTypeDeGraine(TypeDeGraine typeDeGraine) {
		this.typeDeGraine = typeDeGraine;
	}

	@Override
	public String toString() {
		return "Sachet [fournisseur=" + fournisseur + ", typeDeGraine=" + typeDeGraine + ", id=" + id
				+ ", dateDeCreation=" + dateDeCreation + ", poidsEnGrammes=" + poidsEnGrammes + ", prixEnEuros="
				+ prixEnEuros + "]";
	}

}
