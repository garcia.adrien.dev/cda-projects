package fr.humanbooster.ag.gestion_de_sachet.metier;

import java.util.List;

public class Fournisseur {

	private List<Sachet> sachets;
	private Long id;
	private String nom;
	private static Long compteur = 0L;

	public Fournisseur() {

		id = ++compteur;
	}

	public Fournisseur(String nom) {
		this();
		this.nom = nom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Sachet> getSachets() {
		return sachets;
	}

	public void setSachets(List<Sachet> sachets) {
		this.sachets = sachets;
	}

	@Override
	public String toString() {
		return "Fournisseur [sachets=" + sachets + ", id=" + id + ", nom=" + nom + "]";
	}

}
