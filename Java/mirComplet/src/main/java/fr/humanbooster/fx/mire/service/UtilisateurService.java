package fr.humanbooster.fx.mire.service;

import fr.humanbooster.fx.mire.business.Utilisateur;

public interface UtilisateurService {

	Utilisateur ajouterUtilisateur(String nom, String prenom, String email, String motDePasse);

}
