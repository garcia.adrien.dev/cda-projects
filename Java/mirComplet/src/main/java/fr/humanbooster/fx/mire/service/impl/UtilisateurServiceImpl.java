package fr.humanbooster.fx.mire.service.impl;

import java.sql.SQLException;

import fr.humanbooster.fx.mire.business.Utilisateur;
import fr.humanbooster.fx.mire.dao.UtilisateurDao;
import fr.humanbooster.fx.mire.dao.impl.UtilisateurDaoImpl;
import fr.humanbooster.fx.mire.service.UtilisateurService;

public class UtilisateurServiceImpl implements UtilisateurService {

	private UtilisateurDao utilisateurDao = new UtilisateurDaoImpl();
	
	@Override
	public Utilisateur ajouterUtilisateur(String nom, String prenom, String email, String motDePasse) {
		Utilisateur utilisateur = new Utilisateur(nom, prenom, email, motDePasse);
		try {
			return utilisateurDao.create(utilisateur);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
