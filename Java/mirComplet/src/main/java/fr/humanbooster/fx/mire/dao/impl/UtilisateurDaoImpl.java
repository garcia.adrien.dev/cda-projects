package fr.humanbooster.fx.mire.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.humanbooster.fx.mire.business.Utilisateur;
import fr.humanbooster.fx.mire.dao.ConnexionBdd;
import fr.humanbooster.fx.mire.dao.Requetes;
import fr.humanbooster.fx.mire.dao.UtilisateurDao;

public class UtilisateurDaoImpl implements UtilisateurDao {

	private Connection connection;
	
	public UtilisateurDaoImpl() {
		try {
			connection = ConnexionBdd.getConnection();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public Utilisateur create(Utilisateur utilisateur) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_UTILISATEUR, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, utilisateur.getNom());
		ps.setString(2, utilisateur.getPrenom());
		ps.setString(3, utilisateur.getEmail());
		ps.setString(4, utilisateur.getMotDePasse());
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		if (rs.next()) {
			utilisateur.setId(rs.getLong(1));
		}
		return utilisateur;
	}

}
