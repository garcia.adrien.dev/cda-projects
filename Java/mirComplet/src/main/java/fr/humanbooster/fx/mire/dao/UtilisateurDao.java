package fr.humanbooster.fx.mire.dao;

import java.sql.SQLException;

import fr.humanbooster.fx.mire.business.Utilisateur;

public interface UtilisateurDao {

	Utilisateur create(Utilisateur utilisateur) throws SQLException;
}
