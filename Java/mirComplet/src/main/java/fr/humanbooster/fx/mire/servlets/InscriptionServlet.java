package fr.humanbooster.fx.mire.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.mire.business.Utilisateur;
import fr.humanbooster.fx.mire.service.UtilisateurService;
import fr.humanbooster.fx.mire.service.impl.UtilisateurServiceImpl;

/**
 * Servlet implementation class InscriptionServlet
 * 
 * Une servlet est une classe Java capable de traiter une requête HTTP et de forumuler une réponse 
 * La servlet correspond au controleur dans le modèle MVC
 * M (Modèle) correspond à l'ensemble des classes business
 * V (Vues) correspond à l'ensmble des fichers dans src/main/webapp
 * C (Controleur) correspond à l'ensemble des classe dans le package servlet
 */
@WebServlet("/inscription")
public class InscriptionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UtilisateurService utilisateurService = new UtilisateurServiceImpl();
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InscriptionServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// On récupère la donnée saisie dans le champ nom
		String nom = request.getParameter("NOM");
		String prenom = request.getParameter("PRENOM");
		String email = request.getParameter("EMAIL");
		String motDePasse = request.getParameter("MOT_DE_PASSE");
		// On confit la persistance au service
		Utilisateur utilisateur = utilisateurService.ajouterUtilisateur(nom, prenom, email, motDePasse);
		response.getWriter().append(utilisateur.getPrenom() + " souhaite s'inscrire").append(request.getContextPath());
	}

}
