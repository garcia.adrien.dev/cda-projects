"use strict";

const photos = [
  { src: "1.jpg", legend: "Frères pandas" },
  { src: "2.jpg", legend: "Yoga on the top" },
  { src: "3.jpg", legend: "Lever de soleil" },
  { src: "4.jpg", legend: "Ciel étoilé" },
  { src: "5.jpg", legend: "Tea time" },
  { src: "6.jpg", legend: "Ca va peter le bide" },
];

let state = new Object();

/*********************
 *FUNCTION
 *********************/
function toolbarToggle() {
  document.querySelector(".toolbar ul").classList.toggle("hide");

  const icon = document.querySelector("#toolbar-toggle i");
  icon.classList.toggle("fa-arrow-right");
  icon.classList.toggle("fa-arrow-down");
}

function refreshSlider() {
  document.querySelector("#slider img").src =
    "images/" + photos[state.index].src;
  document.querySelector("#slider figcaption").textContent =
    photos[state.index].legend;
}

function nextOne() {
  if (state.index < photos.length - 1) {
    state.index += 1;
  } else {
    state.index = 0;
  }
  refreshSlider();
}

function previousOne() {
  if (state.index > 0) {
    state.index -= 1;
  } else {
    state.index = photos.length - 1;
  }
  refreshSlider();
}

function randomOne() {
  let nb = 0;
  do {
    nb = getRandomInteger(0, 5);
  } while (state.index == nb);
  state.index = nb;
  refreshSlider();
}

function playButton() {
  document.querySelector("#slider-toggle i").classList.toggle("fa-play");
  document.querySelector("#slider-toggle i").classList.toggle("fa-pause");
  if (
    document.querySelector("#slider-toggle i").classList.contains("fa-pause")
  ) {
    state.idInterval = setInterval(nextOne, 2000);
  } else {
    clearInterval(state.idInterval);
    state.intevral = null;
  }
}

/*******************
 *CODE PRINCIPAL
 *******************/

document.addEventListener("DOMContentLoaded", function () {
  state.index = 0;
  refreshSlider();
  document.querySelector("#slider-next").addEventListener("click", nextOne);
  document
    .querySelector("#slider-previous")
    .addEventListener("click", previousOne);
  document.querySelector("#slider-random").addEventListener("click", randomOne);
  document
    .querySelector("#toolbar-toggle")
    .addEventListener("click", toolbarToggle);
  document
    .querySelector("#slider-toggle")
    .addEventListener("click", playButton);
});
