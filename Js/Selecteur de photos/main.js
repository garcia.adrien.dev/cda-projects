"use strict";

/*****************
 *   FUNCTIONS
 ****************/

function selectOne() {
  this.classList.toggle("selected");
  compteur();
}

function compteur() {
  //cible toutes les li qui ont la classe selected -> dans un tableau dont
  let nbSelected = document.querySelectorAll("li.selected").length;

  //on cible le em et on injecte dans son contenu textuel le nb de li sélectionées
  document.querySelector("#total em").textContent = nbSelected;
}

function selectAllim() {
  PHOTOS.forEach((img) => img.classList.add("selected"));
  compteur();
}

function deselectAllim() {
  PHOTOS.forEach((img) => img.classList.remove("selected"));
  compteur();
}

function toggleSelect() {
  if (this.dataset.mode == "select") {
    PHOTOS.forEach((img) => img.classList.add("selected"));
    this.dataset.mode = "deselect";
    this.textContent = "❌Deselectionner tout";
  } else {
    PHOTOS.forEach((img) => img.classList.remove("selected"));
    this.dataset.mode = "select";
    this.textContent = "✅Selectionner tout";
  }
  compteur();
}

/******************
 * CODE PRINCIPALE
 ******************/

const PHOTOS = document.querySelectorAll(".photo-list li");
document.querySelector("#selectAll").addEventListener("click", selectAllim);
document.querySelector("#deselectAll").addEventListener("click", deselectAllim);
document.querySelector("#toggle").addEventListener("click", toggleSelect);

PHOTOS.forEach((img) => img.addEventListener("click", selectOne));

//PHOTOS.addEventListener("click", selectOne);

/**
   * for
   *
    for (let i = 0; i < PHOTOS.length; i++){
        PHOTOS[i].addEventListener("click", selectOne);
    }
   /**
    * for...of
    *
    for (let img of PHOTOS){
        img.addEventListener("click", selectOne);
    }
    /**
     * foreach (2 écritures possibles)
     *
    PHOTOS.forEach(function(img){
      img.addEventListener("click", selectOne);  
    });
*/
